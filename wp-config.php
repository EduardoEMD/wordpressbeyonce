<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'e_wpbeyonce');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '2YxYS$m?kY0G )k=$q9&R`kx{<P!0%WN;ri;^dayi7Rv64qA)}~7@M1TgC,cVcYI');
define('SECURE_AUTH_KEY',  'a@:kHK1nFHpHwu@Vn`5b F.PYTYN#4?*#/Nr2D?PL.s@ZM7o)Bex{#Zv_nM$5}zc');
define('LOGGED_IN_KEY',    'XZ>f%($*]1wl7rx=x$OlLZ9zJ7DR(.Y*c@i_k}H`&/QYQ<VY2Fl6@5J/W]rBL`YP');
define('NONCE_KEY',        '1z2GCJv61]&x:<B}Pm{0Q+K6KKu|z$E$noOiNDPls>^EKs*0CSDOX23O@;<z8Gmy');
define('AUTH_SALT',        'J3H,K7C7yhY+WRp0;^bpxt+CzI3LN0[J%Lw&NwYu;%e<] @~UYOs(P38^?o0Lf&B');
define('SECURE_AUTH_SALT', 'T29b}7m{-cX!8D YZS*QXejSZM*((t<3A_(qw>NTVSDw:9q|&j1e|.~mU17;%W/r');
define('LOGGED_IN_SALT',   'tM!mH1dG2oLr LQ!aA2Twa`)CB%<Ob([lXK!`tb:)-T6>[3,c3)&If(VAS38~cjA');
define('NONCE_SALT',       'l(oo//T^{&5Pci}FHJVf!9W>&e^g,Lsj4,(P$@WULw/p?s%c!XAz/n,1Cg!d*hHn');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
